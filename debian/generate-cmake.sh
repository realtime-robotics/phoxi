#!/bin/sh

# scrippies/generate-cmake.sh

# Create the cmake support file
#
#     ${here}/tmp/${PREFIX}/lib/${DEB_HOST_MULTIARCH}/cmake/${DEB_SOURCE}/${DEB_SOURCE}Config.cmake
#
# based on the contents of '${here}/tmp', the "installation directory" assumed
# to be already populated by 'dh_auto_install' or some similar mechanism.
#
# HOWTO:
#
# * Install this script directly under the 'debian' directory; i.e., as
#   'debian/generate-cmake.sh'.
#
# * Modify your 'debian/rules' target 'override_dh_auto_install' (adding it if
#   necessary) to call './debian/generate-cmake.sh' _after_ 'debian/tmp' has
#   been populated with headers and shared objects.
#
# * Modify your 'debian/rules' target 'override_dh_install' (adding it if
#   necessary) to let the '*-dev' package claim any path of the form
#   'usr/lib/*/cmake/*'.
#
# BACKSTORY:
#
# I wrote this script to automate the generation/emplacement within a Debian
# package of a minimal '*Config.cmake' support file when no other means are
# feasible. A typical use case: Debian packaging for a proprietary artifact
# that ships '*Config.cmake' support files that are either completely absent or
# hopelessly broken.
#
# In a completely unrelated matter, I'm sure that whatever '*.cmake' files
# Photoneo packed into that fancy 'PhotoneoPhoXiControlInstaller-*-STABLE.run'
# self-extracting archive will work just fine for you. It's probably not broken
# at all. You're probably just holding it wrong.

set -eu

export LC_ALL="${LC_ALL:-C}"

this="$(realpath "$0")"
readonly this="${this}"
here="$(dirname "${this}")"
readonly here="${here}"
whatami="$(basename "${this}")"
readonly whatami="${whatami}"

log() { echo "${whatami}"'['"$$"']:' "$@" >&2; }
error() { log "ERROR:" "$@"; }
warning() { log "WARNING:" "$@"; }
info() { log "INFO:" "$@"; }
die() {
    error "$@"
    usage >&2
    exit 1
}

usage() {
    cat <<EOF

Usage: $0 [OPTION]... [-- [FIND_ARG]...]
Generate content for cmake packaging support.

Options:

    -h    print usage and exit

Examples:

    \$ $0 -h

    \$ $0 -- ! -name '*_Debug.so.*'

Used Environment Variables (with defaults)

    PREFIX

        /usr

    DEB_HOST_MULTIARCH

        \$(dpkg-architecture --query DEB_HOST_MULTIARCH)

    DEB_SOURCE

        \$(dpkg-parsechangelog --file=${here}/changelog --show-field=Source)

    DEB_VERSION_UPSTREAM

        \$(dpkg-parsechangelog --file=${here}/changelog --show-field=Version \\
            | sed -e 's/-[^-]*$//' -e 's/^[0-9]*://')

Notes:

    * This script only works properly when installed to the 'debian' directory.

    * This script emits content suitable for the cmake support file

        \${PREFIX}/lib/\${DEB_HOST_MULTIARCH}/cmake/\${DEB_SOURCE}/\${DEB_SOURCE}Config.cmake

    * This script emits content based on what it finds under

        ${here}/tmp/

      and must be called _after_ 'dh_auto_install' populates that directory.

    * FIND_ARGs are used in the 'find' call that enumerates shared objects.

EOF
}

################################################################################
################################################################################
################################################################################

while getopts ":h" opt; do
    case "${opt}" in
        h)
            usage
            exit 0
            ;;
        :) die "missing argument: -${OPTARG}" ;;
        \?) die "bad option: -${OPTARG}" ;;
    esac
done
shift "$((OPTIND - 1))"

################################################################################

# PREFIX
PREFIX="${PREFIX:-/usr}"
export PREFIX
info PREFIX: "${PREFIX}"

# DEB_HOST_MULTIARCH
DEB_HOST_MULTIARCH="${DEB_HOST_MULTIARCH:-$(
    dpkg-architecture --query DEB_HOST_MULTIARCH

)}"
export DEB_HOST_MULTIARCH
info DEB_HOST_MULTIARCH: "${DEB_HOST_MULTIARCH}"

# DEB_SOURCE
DEB_SOURCE="${DEB_SOURCE:-$(
    dpkg-parsechangelog --file="${here}"/changelog --show-field=Source
)}"
export DEB_SOURCE
info DEB_SOURCE: "${DEB_SOURCE}"

# DEB_VERSION_UPSTREAM
DEB_VERSION_UPSTREAM="${DEB_VERSION_UPSTREAM:-$(
    dpkg-parsechangelog --file="${here}"/changelog --show-field=Version \
        | sed -e 's/-[^-]*$//' -e 's/^[0-9]*://'
)}"
export DEB_VERSION_UPSTREAM
info DEB_VERSION_UPSTREAM: "${DEB_VERSION_UPSTREAM}"

################################################################################

# Check if dh_auto_install has populated 'debian/tmp'.
if ! [ -d "${here}"/tmp/"${PREFIX}"/include ]; then
    die missing "${here}"/tmp/"${PREFIX}"/include
fi
if ! [ -d "${here}"/tmp/"${PREFIX}"/lib/"${DEB_HOST_MULTIARCH}" ]; then
    die missing "${here}"/tmp/"${PREFIX}"/lib/"${DEB_HOST_MULTIARCH}"
fi
if ! libraries="$(
    cd "${here}"/tmp/"${PREFIX}"/lib/"${DEB_HOST_MULTIARCH}"
    find \
        . \
        "$@" \
        -type f \
        -execdir sh -c 'objdump -p "$0" 2>/dev/null | grep -Fq SONAME' {} \; \
        -exec realpath --canonicalize-missing "${PREFIX}"/lib/"${DEB_HOST_MULTIARCH}"/{} \; \
        | sort \
        | grep -Ex '[[:graph:]]+'
)"; then
    die missing libraries: "${here}"/tmp/"${PREFIX}"/lib/"${DEB_HOST_MULTIARCH}"
fi
#info libraries: "$(echo && echo "${libraries}" | sed 's/^/  /g')"
install_path="$(
    printf \
        '%s/lib/%s/cmake/%s/%sConfig.cmake' \
        "${PREFIX}" \
        "${DEB_HOST_MULTIARCH}" \
        "${DEB_SOURCE}" \
        "${DEB_SOURCE}"
)"
#info install_path: "${install_path}"

################################################################################

mkdir -p "$(dirname "${here}"/tmp/"${install_path}")"
cat >"${here}"/tmp/"${install_path}" <<EOF
# ${install_path}

set(${DEB_SOURCE}_FOUND TRUE)
set(${DEB_SOURCE}_VERSION ${DEB_VERSION_UPSTREAM})
set(${DEB_SOURCE}_INCLUDE_DIRS ${PREFIX}/include)
set(${DEB_SOURCE}_LIBRARIES
$(echo "${libraries}" | sed 's/^/  /')
)
EOF

info wrote: \
    "${here}"/tmp/"${install_path}": \
    "$(echo && cat "${here}"/tmp/"${install_path}")"
